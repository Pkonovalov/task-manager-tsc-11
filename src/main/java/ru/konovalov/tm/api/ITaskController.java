package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Task;

public interface ITaskController {
    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskById();

    void showTask(Task task);

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

}
