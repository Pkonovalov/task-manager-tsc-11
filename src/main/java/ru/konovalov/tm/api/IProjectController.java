package ru.konovalov.tm.api;

import ru.konovalov.tm.model.Project;

public interface IProjectController {
    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProjectById();

    void showProject(Project project);

    void showProjectByName();

    void removeProjectByIndex();

    void removeProjectById();

    void removeProjectByName();

    void updateProjectByIndex();

    void updateProjectById();

}
